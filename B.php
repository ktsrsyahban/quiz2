<?php
function perolehan_medali($kemp)
{
    $out = [
        ["negara" => "Indonesia", "emas" => 0, "perak" => 0, "perunggu" => 0],
        ["negara" => "India", "emas" => 0, "perak" => 0, "perunggu" => 0],
        ["negara" => "Korea Selatan", "emas" => 0, "perak" => 0, "perunggu" => 0],
    ];
    if (!$kemp) {
        return "no data";
    }
    for ($i = 0; $i < count($kemp); $i++) {
        for ($j = 0; $j < count($out); $j++) {
            if ($kemp[$i][0] == $out[$j]["negara"]) {
                $out[$j][$kemp[$i][1]]++;
            }
        }
    }
    return print_r($out);
}

perolehan_medali(
    array(
        array('Indonesia', 'emas'),
        array('India', 'perak'),
        array('Korea Selatan', 'emas'),
        array('India', 'perak'),
        array('India', 'emas'),
        array('Indonesia', 'perak'),
        array('Indonesia', 'emas'),
    )
);
